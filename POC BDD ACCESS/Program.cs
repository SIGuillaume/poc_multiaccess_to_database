﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace POC_BDD_ACCESS
{
    static class Program
    {
       
        [STAThread]
        static void Main()
        {
            Interface();
        }
       
        public static void Interface()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

    }
}



