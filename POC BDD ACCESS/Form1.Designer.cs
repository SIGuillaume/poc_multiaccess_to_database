﻿namespace POC_BDD_ACCESS
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.TbxPassword = new System.Windows.Forms.TextBox();
            this.TbxLogin = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LbxUsers = new System.Windows.Forms.ListBox();
            this.LaunchThread = new System.Windows.Forms.Button();
            this.labelThread2 = new System.Windows.Forms.Label();
            this.labelThread1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.TbxPassword);
            this.groupBox1.Controls.Add(this.TbxLogin);
            this.groupBox1.Location = new System.Drawing.Point(12, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 216);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inscription";
           // this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "MDP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Login";
           // this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(156, 114);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 77);
            this.button2.TabIndex = 2;
            this.button2.Text = "S\'inscrire";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TbxPassword
            // 
            this.TbxPassword.Location = new System.Drawing.Point(146, 82);
            this.TbxPassword.Name = "TbxPassword";
            this.TbxPassword.Size = new System.Drawing.Size(133, 26);
            this.TbxPassword.TabIndex = 4;
            this.TbxPassword.Text = "pass";
            // 
            // TbxLogin
            // 
            this.TbxLogin.Location = new System.Drawing.Point(146, 40);
            this.TbxLogin.Name = "TbxLogin";
            this.TbxLogin.Size = new System.Drawing.Size(133, 26);
            this.TbxLogin.TabIndex = 3;
            this.TbxLogin.Text = "login";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LbxUsers);
            this.groupBox2.Location = new System.Drawing.Point(782, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(306, 234);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Utilisateur";
           // this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // LbxUsers
            // 
            this.LbxUsers.FormattingEnabled = true;
            this.LbxUsers.ItemHeight = 20;
            this.LbxUsers.Location = new System.Drawing.Point(50, 25);
            this.LbxUsers.Name = "LbxUsers";
            this.LbxUsers.Size = new System.Drawing.Size(216, 344);
            this.LbxUsers.TabIndex = 0;
            // 
            // LaunchThread
            // 
            this.LaunchThread.Location = new System.Drawing.Point(85, 25);
            this.LaunchThread.Name = "LaunchThread";
            this.LaunchThread.Size = new System.Drawing.Size(133, 62);
            this.LaunchThread.TabIndex = 8;
            this.LaunchThread.Text = "2 Thread";
            this.LaunchThread.UseVisualStyleBackColor = true;
            this.LaunchThread.Click += new System.EventHandler(this.LaunchThread_Click);
            // 
            // labelThread2
            // 
            this.labelThread2.AutoSize = true;
            this.labelThread2.Location = new System.Drawing.Point(185, 130);
            this.labelThread2.Name = "labelThread2";
            this.labelThread2.Size = new System.Drawing.Size(72, 20);
            this.labelThread2.TabIndex = 9;
            this.labelThread2.Text = "Thread 2";
            // 
            // labelThread1
            // 
            this.labelThread1.AutoSize = true;
            this.labelThread1.Location = new System.Drawing.Point(30, 130);
            this.labelThread1.Name = "labelThread1";
            this.labelThread1.Size = new System.Drawing.Size(72, 20);
            this.labelThread1.TabIndex = 10;
            this.labelThread1.Text = "Thread 1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelThread1);
            this.groupBox3.Controls.Add(this.LaunchThread);
            this.groupBox3.Controls.Add(this.labelThread2);
            this.groupBox3.Location = new System.Drawing.Point(382, 35);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(306, 234);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Accés simultané";
            //this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 328);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
           // this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox TbxPassword;
        private System.Windows.Forms.TextBox TbxLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox LbxUsers;
        private System.Windows.Forms.Button LaunchThread;
        private System.Windows.Forms.Label labelThread2;
        private System.Windows.Forms.Label labelThread1;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

