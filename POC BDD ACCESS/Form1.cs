﻿using System;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;
using System.Drawing;

namespace POC_BDD_ACCESS
{
    public partial class Form1 : Form
    {
        string dbPath = "C:/Users/gsibot/source/repos/POC BDD ACCESS/POC BDD ACCESS/bin/Debug/POCBDD.db";
        //string dbPath = "POCBDD.db";
        public Form1()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            InsertSQL();
        }

        private void LaunchThread_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Main thread: Start a second thread.");
            Thread t1 = new Thread(new ThreadStart(ThreadProc1));
            Thread t2 = new Thread(new ThreadStart(ThreadProc2));
            t1.Start();
            t2.Start();
            ////Console.WriteLine("Main thread: Call Join(), to wait until ThreadProc ends.");
            ////t1.Join();
            ////t2.Join();
            ////Console.WriteLine("Main thread: ThreadProc.Join has returned.  Press Enter to end program.");
            ////Console.ReadLine();
        }
        public void ThreadProc1()
        {
            string name = "Thread1";
            //Voyant d'activité
            Console.WriteLine("Thread1 Works");
            for (int i = 0; i < 4; i++)
            {
                labelThread1.BackColor = Color.Green;
                Console.WriteLine("Main thread: Do some work.");
                Thread.Sleep(100);
                labelThread1.BackColor = Color.Yellow;
                Thread.Sleep(100);
            }
            labelThread1.BackColor = Color.Black;
            DefaultInsert(name);
            
        }
        public void ThreadProc2()
        {
           string name = "Thread2";
            //Voyant d'activité
            Console.WriteLine("Thread2 Works");
            for (int i = 0; i < 4; i++)
            {
                labelThread2.BackColor = Color.Green;
                Console.WriteLine("second thread: Do some work.");
                Thread.Sleep(100);
                labelThread2.BackColor = Color.Yellow;
                Thread.Sleep(100);
            }
            labelThread2.BackColor = Color.Black;
            DefaultInsert(name);
           
        }  
        public void InsertSQL()
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=" + dbPath);
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(connection);
            command.CommandText = "INSERT INTO Personne (login, password ) VALUES (@login, @password)";
            SQLiteParameter p1 = new SQLiteParameter("login", TbxLogin.Text);
            SQLiteParameter p2 = new SQLiteParameter("password", TbxPassword.Text);

            command.Parameters.Add(p1);
            command.Parameters.Add(p2);
            command.ExecuteNonQuery();

            TbxLogin.Clear();
            TbxPassword.Clear();
            MessageBox.Show("inscription effectué");

            command.CommandText = "SELECT * From Personne";
            SQLiteDataReader reader = command.ExecuteReader();
            LbxUsers.Items.Clear();
            while (reader.Read())
            {
                LbxUsers.Items.Add(reader["login"]);
            }
            connection.Close();
        }
        public void DefaultInsert(string name)
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=" + dbPath);
            connection.Open();
            SQLiteCommand command = new SQLiteCommand(connection);
            command.CommandText = "INSERT INTO Personne (login, password ) VALUES (@login, @password)";
            //command.Parameters.AddWithValue("@login", "thread");
            command.Parameters.AddWithValue("@login", name);
            command.Parameters.AddWithValue("@password", "passthread");

            command.ExecuteNonQuery();
            connection.Close();
        }


    }
}
